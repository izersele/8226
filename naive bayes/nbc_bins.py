'''
author: izere gisele
Description:
 Implement NBC model to predict
 Observing model dependency to binning some attribute in varying bin size
Requirements:
 Python3, pandas, numpy, discretize.py
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
from pathlib import Path
# importing from files in the folder
import discretize
import nbc
from split import partitionning

def plotting(B,train, test):
    import matplotlib.pyplot as plt
    # red dashes, blue squares and green triangles
    fig = plt.figure()
    plt.plot(B, train, '-.g')
    plt.plot(B, test, '-k')

    plt.ylabel('Bin sizes')
    plt.ylabel('Accuracy')
    plt.title('Bin sizes vs NBC Accuracy')
    plt.axis(ymax=.9, ymin=.4)
    plt.legend(('Training Accuracy','Testing Accuracy'))
    plt.grid(axis='y', linestyle='-')
    plt.show()

def Btesting():
    if len(sys.argv) <2:
        print('File not found!!\n System exiting ...')
        sys.exit()

    df = discretize.load_csv(sys.argv[1])
    if df.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()
    #NAIVE BAYES
    def NBC(t_frac=1):
        N=0 #number of labels in the dataset
        attr_probs = {}
        attr_lab = []

        #divide training set into training and testing set frac = 1
        xtrain, xtest = partitionning(train,t_frac,50)
        attr= xtrain.columns.values

        #P(king) prob computation
        for head in xtrain.columns.values:
            attr_probs[head]=d5_1.likelihood(xtrain[head])

        labels = sorted(list(attr_probs['decision']))
        N = len(labels)

        #P(king/Face) probab computation
        for label in labels:
            mask = xtrain['decision'] ==label
            xlabel =xtrain[mask].drop(columns=['decision'])
            attr_={}
            for head in xlabel.columns.values:
                attr_[head]=d5_1.likelihood(xlabel[head])

            attr_lab.append(attr_)

        #test sample label prediction
        ypred = d5_1.predictions(np.array(xtrain),attr,N,attr_probs,attr_lab)
        atrain=d5_1.accuracy(np.array(xtrain['decision']),np.array(ypred))
        print('Training accuracy:',atrain)
        ypred = d5_1.predictions(np.array(test),attr,N,attr_probs,attr_lab)
        atest=d5_1.accuracy(np.array(test['decision']),np.array(ypred))
        print('Testing accuracy:',atest)
        return atrain, atest

    # compute new bins
    B = [2, 5, 10, 50, 100, 200]
    acc_train=[]
    acc_test =[]
    attr = discretize.continuous_valued_columns
    attr.append('decision')
    for b in B:
        dt = df.copy()
        print('*** Bin size:',b)
        for cval in attr:
            dt[cval]=discretize.freq_count(dt[cval],b)[0]
            #print(cval,',',discretize.freq_count(dt[cval],b)[1])

        train, test = partitionning(dt,.7,47)
        # run predictor

        #train = train[attr]
        #test=test[attr]
        c,d=NBC()
        acc_train.append(c)
        acc_test.append(d)

    plotting(B,acc_train,acc_test)

    ###end MAIN

if __name__== "__main__":
    Btesting()
