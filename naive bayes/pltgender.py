'''
author: izere gisele
Description:
  visualizing male and female preferences using horizontal bar chart
Requirements:
  Python3, pandas, numpy
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
import sys


preference_scores_of_participant = ['attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important']
preference_scores_of_partner=['pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests']
continous_valued_columns =['importance_same_race','importance_same_religion', 'pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests', 'attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important', 'attractive' ,'sincere', 'intelligence','funny' ,'ambition' ,'attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner','sports' ,'tvsports' ,'exercise' ,'dining','museums' ,'art' ,'hiking', 'gaming' ,'clubbing', 'reading', 'tv' ,'theater','movies', 'concerts', 'music' ,'shopping', 'yoga', 'interests_correlate','expected_happy_with_sd_people', 'like']
rating_of_partner_from_participant =['attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner']

#reading data from file
def load_csv(file):
    return pd.read_csv(file)

#find mean of columns
def fmean(dt,col):
    mval = []
    for co in col:
        mval.append(dt[co].mean())
    return mval
#plotting bar horizontally: get chart time, attribut, and data
def plotting(title,attr,male, female):
    y_pos = np.arange(len(attr))
    #print(male)
    rect1=plt.barh(y_pos,male,0.35, alpha=0.5, color ='#3197e0', label='Male')
    rect2=plt.barh(y_pos+0.35,female,0.35, alpha=0.5, color ='#fda641', label='Female')
    plt.yticks(y_pos, attr)
    plt.title(title)
    plt.legend()
    plt.tight_layout()
    plt.show()

def main():
    if len(sys.argv) <2:
        print('File not found!!\n System exiting ...')
        sys.exit()

    dt = load_csv(sys.argv[1])
    #testing dataframe is not empty
    #print(dt.columns.values)
    if dt.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()

    #dividing data set by gender
    mask = dt['gender'] =='male'
    male =dt[mask]
    female = dt[~mask]
    #Object that 77 of 6744 sum up higher than 99.9
    #x1_sum = np.sum(dt[preference_scores_of_participant],axis=1)
    #print(len([x for x in x1_sum if x <99.9]),len(dt))
    #computing means
    Mmeans =fmean(male,preference_scores_of_participant)
    Fmeans=fmean(female,preference_scores_of_participant)
    #creating bar plot fo mean
    plotting('preference_scores_of_participant',preference_scores_of_participant,Mmeans,Fmeans)

    Mmeans =fmean(male,preference_scores_of_partner)
    Fmeans=fmean(female,preference_scores_of_partner)
    #creating bar plot fo mean
    plotting('preference_scores_of_partner',preference_scores_of_partner,Mmeans,Fmeans)

    ###end MAIN

if __name__== "__main__":
    main()
