'''''
author: izere gisele
Description:
 visualizing partner rating by participant using scatterplot
Requirements: Python3, pandas, numpy
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys


preference_scores_of_participant = ['attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important']
preference_scores_of_partner=['pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests']
continuous_valued_columns =['age','age_o','importance_same_race','importance_same_religion', 'pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests','attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important','attractive' ,'sincere', 'intelligence','funny' ,'ambition','attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner','sports' ,'tvsports' ,'exercise' ,'dining','museums' ,'art' ,'hiking', 'gaming' ,'clubbing', 'reading', 'tv' ,'theater','movies', 'concerts', 'music' ,'shopping', 'yoga', 'interests_correlate','expected_happy_with_sd_people', 'like']
rating_of_partner_from_participant =['attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner']

#reading data from file
def load_csv(file):
    return pd.read_csv(file)

# find unique values of the attribute
def unique_vals(data):
    vals = list(set([(x) for x in data]))
    return (vals)
# compute frequencies of values of a given col
def freq_count(dt):
    val={}
    for i in dt:
        if i not in val:
            val[i] = 0.
        val[i] +=1

    return val

# compute occurences of a given val in given col of data
def frequency(dat,val):
    freq =0
    for v in dat:
        if v == val:
            freq +=1
    return float(freq)/len(dat)

#scatter plotting data
def plotting(val,attr,title,n):
    plt.figure()
    ax = plt.subplot(111)
    mark= [0,'|','+','1','*','3','^']
    for mrk,lab,bv in zip(mark,attr,val):
        x= sorted(bv)
        y =[float(bv[i]) for i in x] # normalize with bv[i])/n
        #ax.plot(x, y, 'x-', lw=1)
        ax.scatter(x,y, marker =mrk, label =lab)

    ax.legend()
    plt.title(title)
    plt.show()

def main():
    if len(sys.argv) <2:
        print('File not found!!\n System exiting ...')
        sys.exit()

    dt = load_csv(sys.argv[1])
    #testing dataframe is not empty
    #print(dt.columns.values)
    if dt.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()

    print('\nDistinct value in attractive_partner: %d'%len(unique_vals(dt['attractive_partner'])))
    print('Success rate of val 7 in attractive_partner: %.2f'%frequency(dt['attractive_partner'],7))
    print('Bins and frequency per bin of attractive_partner:',freq_count(dt['attractive_partner']))
    #rating_of_partner_from_participant distribution
    rating_bins =[]
    for h in rating_of_partner_from_participant:
        bin= freq_count(dt[h])
        rating_bins.append(bin)


    #print(rating_bins[0])
    plotting(rating_bins,rating_of_partner_from_participant,'rating_of_partner_from_participant Distributions',len(dt))

    ###end MAIN

if __name__== "__main__":
    main()
