'''
author: izere gisele
Description:
    Implement NBC model to predit
    Using all attributes to predict whether
    participant wants to go on a second date or not
Requirements:
 Python3, pandas, numpy
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
import sys
from pathlib import Path

#attribute sub-sets
preference_scores_of_participant = ['attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important']
preference_scores_of_partner=['pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests']
continuous_valued_columns =['age','age_o','importance_same_race','importance_same_religion', 'pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests','attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important','attractive' ,'sincere', 'intelligence','funny' ,'ambition','attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner','sports' ,'tvsports' ,'exercise' ,'dining','museums' ,'art' ,'hiking', 'gaming' ,'clubbing', 'reading', 'tv' ,'theater','movies', 'concerts', 'music' ,'shopping', 'yoga', 'interests_correlate','expected_happy_with_sd_people', 'like']
rating_of_partner_from_participant =['attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner']

#reading data from file
def load_csv(file):
    f= Path(file)
    if f.is_file():
        return pd.read_csv(file)

    else:
        print('File \'%s\' does not exist.'%file)
        sys.exit()

# compute values probabilities of a given attr given label
def likelihood(dt):
    freq={}
    for vv in dt:
        if vv not in freq:
            freq[vv]=0.
        freq[vv] +=1
    for fr in freq:
        freq[fr]= round(freq[fr]/len(dt),2)
    return freq

#partion data set into training and testing set
#rv: random states, frac: fraction in training set
def partitionning(dt,frac, rv):
    trainset= dt.sample(frac=frac, random_state=rv)
    testset= dt.loc[~dt.index.isin(trainset.index)]
    return trainset,testset

def accuracy(y,ypred):
    corr = 0
    Ny=len(y)
    for k in range(Ny):
        if y[k] == ypred[k]:
            corr +=1
    return round(float(corr)/Ny,2)

def bestlabel(prob):
    label=0
    p=prob[0]
    for l in range(1,len(prob)):
        if p < prob[l]:
            p=prob[l]
            label=l
    return label

def predict(x,attr,N,attr_probs,attr_lab):

    xprob=[1. for i in range(N)]

    for i in range(N):
        for head in range(len(x)-1):
            keys= attr_lab[i][attr[head]]
            if x[head] not in keys :
                xprob[i] *=0.
                break
            xprob[i]*=attr_lab[i][attr[head]][x[head]]
        #divide prob label
        #if attr_probs['decision'][i] > 0.0:
            #xprob[i]=xprob[i]/attr_probs['decision'][i]
    gprob = 1.
    for head in range(len(x)-1):
        keys= attr_probs[attr[head]]
        if x[head] not in keys :
            gprob *=0.
            break
        gprob *= attr_probs[attr[head]][x[head]]

    return (np.multiply(np.array(xprob),gprob)) #np.around

def predictions(dat,attr,N,attr_probs,attr_lab):
    ypred=[]
    for x in dat:
        p=predict(x,attr,N,attr_probs,attr_lab)
        ypred.append(bestlabel(p))
    return ypred

def NBC(t_frac=1):
    N=0 #number of labels in the dataset
    attr_probs = {}
    attr_lab = []
    #open 'trainingSet.csv','testSet.csv'
    files=['trainingSet.csv','testSet.csv']
    dt= load_csv(files[0])
    dtest= load_csv(files[1])
    # test whether dataframe is not empty for training
    if dt.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()

    #divide training set into training and testing set frac = 1
    xtrain, xtest = partitionning(dt,t_frac,50)
    attr= xtrain.columns.values

    #P(king) prob computation
    for head in xtrain.columns.values:
        attr_probs[head]=likelihood(xtrain[head])

    labels = sorted(list(attr_probs['decision']))
    N = len(labels)

    #P(king/Face) probab computation
    for label in labels:
        mask = xtrain['decision'] ==label
        xlabel =xtrain[mask].drop(columns=['decision'])
        attr_={}
        for head in xlabel.columns.values:
            attr_[head]=likelihood(xlabel[head])

        attr_lab.append(attr_)

    #test sample label prediction
    ypred = predictions(np.array(xtrain),attr,N,attr_probs,attr_lab)
    print('Training accuracy:',accuracy(np.array(xtrain['decision']),np.array(ypred)))
    ypred = predictions(np.array(dtest),attr,N,attr_probs,attr_lab)
    print('Testing accuracy:',accuracy(np.array(dtest['decision']),np.array(ypred)))

    ###end MAIN

if __name__== "__main__":
    NBC()
    '''
    if len(sys.argv) <2:
        print('\nMissing value: Value of t-frac not found\n Program will run with default t-frac= 1\n')
        NBC()
    elif float(sys.argv[1]) >1. or float(sys.argv[1]) <=0.:
        print('\nInvalid value: Value of t-frac out of range (0,1]\nProgram will run with default t-frac= 1\n')
        NBC()
    else:
        NBC(round(float(sys.argv[1]),2))
    '''
