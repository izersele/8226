'''
author: izere gisele
Description:
 discretizing continuous values of a subset of attributes into 5 bins
Requirements:
 Python3, pandas, numpy, nbc.py
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import sys


preference_scores_of_participant = ['attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important']
preference_scores_of_partner=['pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests']
continuous_valued_columns =['age','age_o','importance_same_race','importance_same_religion', 'pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests','attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important','attractive' ,'sincere', 'intelligence','funny' ,'ambition','attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner','sports' ,'tvsports' ,'exercise' ,'dining','museums' ,'art' ,'hiking', 'gaming' ,'clubbing', 'reading', 'tv' ,'theater','movies', 'concerts', 'music' ,'shopping', 'yoga', 'interests_correlate','expected_happy_with_sd_people', 'like']
rating_of_partner_from_participant =['attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner']

#reading data from file
def load_csv(file):
    return pd.read_csv(file)

# find unique values of the attribute
def unique_vals(data):
    vals = list(set([int(x) for x in data]))
    return (vals)

# compute frequencies of values of a given col
def freq_count(dt,N=5):

    N -=1
    newval=[]
    vmin = round(dt.min(),2)
    vmax=dt.max()

    if math.isnan(vmin) and math.isnan(vmax):
        bins=0
        for vv in dt:
            bins +=1
            newval.append(0)
        return newval,[bins]
    if vmin == vmax or N <0:
        #attr set of a single value
        for vv in dt:
            bins[vmin] +=1
            newval.append(vmin)
        return newval,[bins]

    step=(round((vmax - vmin)/N,2))
    #print(step)

    #print(sorted(bins))
    temp=0
    if step == 0:
        bins=0
        for vv in dt:
            bins +=1
            newval.append(vmin)
        return newval,[bins]
    bins={}
    for i in range(N+1):
        bins[(vmin+round(i*step,2))]=0
    last =list(bins)[-1]
    for vv in dt:
        yval= int(float(vv-vmin)/step)
        temp =(vmin+round(yval*step,2))
        if temp >last:
            temp=last
        newval.append(temp)
        bins[temp] +=1

    return newval,list(bins.values())


def main():
    if len(sys.argv) <2:
        print('File not found!!\n System exiting ...')
        sys.exit()

    dt = load_csv(sys.argv[1])
    #testing dataframe is not empty
    #print(dt.columns.values)
    if dt.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()

    #for attr in rating_of_partner_from_participant:
    #print('Length of continuous_valued_columns:',len(continuous_valued_columns))
    #print('sincere_important ',': ',freq_count(dt['sincere_important'])[1])

    for cval in continuous_valued_columns:
        dt[cval], dating_bins=freq_count(dt[cval])
        print(cval,': ',dating_bins)

    #print(pd.DataFrame(dating_bins))
    #pd.cut
    #writing to a csv_file

    if len(sys.argv) < 3:
        filename ='dating-binned.csv'
        print('Output file not found!!\nSystem created and stored output data for you in outfile %s...\n'%filename)
        dt.to_csv(filename,index=False)
    else:
        dt.to_csv(sys.argv[2],index=False)

    ###end MAIN

if __name__== "__main__":
    main()
