'''
author: izere gisele
Description:
 Use NBC model to predict model accuracy depending on the size
 of the training data

Requirements:
 Python3, pandas, numpy, nbc.py
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
from pathlib import Path
# importing from files in the folder
import nbc

def plotting(F,train, test, valid):
    import matplotlib.pyplot as plt
    # red dashes, blue squares and green triangles
    fig = plt.figure()
    plt.plot(F, train, '-.g')
    plt.plot(F, test, '-k')
    plt.plot(F, valid, '--r')

    plt.ylabel('t_frac values')
    plt.ylabel('Accuracy')
    plt.title('T_frac values vs NBC Accuracy')
    plt.axis(ymax=.9, ymin=.4)
    plt.legend(('Training Accuracy','Testing Accuracy','Validation Accuracy'))
    plt.grid(axis='y', linestyle='-')
    plt.show()

def NBC(t_frac=1):
    N=0 #number of labels in the dataset
    attr_probs = {}
    attr_lab = []
    #open 'trainingSet.csv','testSet.csv'
    files=['trainingSet.csv','testSet.csv']
    dt= d5_1.load_csv(files[0])
    dtest= d5_1.load_csv(files[1])
    # test whether dataframe is not empty for training
    if dt.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()

    #divide training set into training and testing set frac = 1
    xtrain, xtest = d5_1.partitionning(dt,t_frac,50)
    attr= xtrain.columns.values

    #P(king) prob computation
    for head in xtrain.columns.values:
        attr_probs[head]=d5_1.likelihood(xtrain[head])

    labels = sorted(list(attr_probs['decision']))
    N = len(labels)

    #P(king/Face) probab computation
    for label in labels:
        mask = xtrain['decision'] ==label
        xlabel =xtrain[mask].drop(columns=['decision'])
        attr_={}
        for head in xlabel.columns.values:
            attr_[head]=d5_1.likelihood(xlabel[head])

        attr_lab.append(attr_)

    #test sample label prediction
    ypred = d5_1.predictions(np.array(xtrain),attr,N,attr_probs,attr_lab)
    acc_train=d5_1.accuracy(np.array(xtrain['decision']),np.array(ypred))
    if not xtest.empty:
        ypred = d5_1.predictions(np.array(xtest),attr,N,attr_probs,attr_lab)
        acc_test=d5_1.accuracy(np.array(xtest['decision']),np.array(ypred))
    else:
        acc_test=acc_train
    ypred = d5_1.predictions(np.array(dtest),attr,N,attr_probs,attr_lab)
    acc_valid=d5_1.accuracy(np.array(dtest['decision']),np.array(ypred))

    return acc_train, acc_test, acc_valid
    ###end MAIN
def main():
    F = [0.01, 0.1, 0.2, 0.5, 0.6, 0.75, 0.9, 1]
    acc_train =[]
    acc_test=[]
    acc_valid=[]
    for f in range(len(F)):
        a,b,c= NBC(F[f])
        #print('****F values: %.2f\n acc_train: %.2f\n acc_test: %.2f\n acc_valid:%.2f'%(f,a,b,c))
        acc_train.append(a)
        acc_test.append(b)
        acc_valid.append(c)
    plotting(F,acc_train, acc_test, acc_valid)
if __name__== "__main__":
    main()
