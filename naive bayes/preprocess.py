'''
author: izere gisele
Description:
 preprocesing dating-full dataset and return dating.csv set
Requirements:
 Python3, pandas, numpy, nbc.py
'''

import pandas as pd
import numpy as np
import sys
from collections import OrderedDict

# attr subsets
preference_scores_of_participant = ['attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important']
preference_scores_of_partner=['pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests']
continuous_valued_columns =['age','age_o','importance_same_race','importance_same_religion', 'pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests','attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important','attractive' ,'sincere', 'intelligence','funny' ,'ambition','attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner','sports' ,'tvsports' ,'exercise' ,'dining','museums' ,'art' ,'hiking', 'gaming' ,'clubbing', 'reading', 'tv' ,'theater','movies', 'concerts', 'music' ,'shopping', 'yoga', 'interests_correlate','expected_happy_with_sd_people', 'like']
rating_of_partner_from_participant =['attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner']

#reading data from file
def load_csv(file):
    return pd.read_csv(file)

#format cell and return the # of cell formatted
def format_cell(dt):
    ncell= 0
    headers = ['race','race_o','field']
    for hd in headers:
        for dx in dt[hd]:
            for c in dx:
                if c in '\' \"':
                    ncell +=1
                    break;
        dt[hd]= dt[hd].str.strip('\'  \"')
    print('Quotes removed from ',ncell, ' cells.')

#change to lower case cell in field
def lowercase(dt):
    ncell= 0
    for dx in dt['field']:
        for c in dx:
            if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                ncell +=1
                break;
    dt['field']= dt['field'].str.lower()
    print('Standardized ',ncell, ' cells to lower case.')

# find unique values in fields
def unique_vals(data,col):
    vals = list(set([x for x in data[col]]))
    return sorted(vals)

# label encoding: get values and encode them in numerical values
def lencode(dt):
    headers = ['gender','race','race_o','field']
    ssize = dt['gender'].size
    for h in headers:
        enc_set = unique_vals(dt,h)
        for x in range(ssize):
            dt[h][x] = enc_set.index(dt[h][x])
    dt[['gender','race','race_o','field']] = dt[['gender','race','race_o','field']].apply(pd.to_numeric)

#finding value code
def findcode(dt,col,val):
    enc_set = unique_vals(dt,col)
    return enc_set.index(val)

#find mean of columns
def fmean(dt,col):
    #x1_sum = np.sum(dt[col],axis=1)
    #print(x1_sum)
    for co in col:
        print('Mean of %s : %.2f' % (co,dt[co].mean()))

def main():
    if len(sys.argv) <2:
        print('File not found!!\n System exiting ...')
        sys.exit()

    dt = load_csv(sys.argv[1])
    #testing dataframe is not empty
    #print(dt.columns.values)
    if dt.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()



    #Solutions to question 1
    format_cell(dt)
    lowercase(dt)
    print('\nValue assigned for male in column gender: ',findcode(dt,'gender','male'))
    print('Value assigned for European/Caucasian-American in column race: ',findcode(dt,'race','European/Caucasian-American'))
    print('Value assigned for Latino/Hispanic American in column race_o: ',findcode(dt,'race_o','Latino/Hispanic American'))
    print('Value assigned for law in column field: ', findcode(dt,'field','law'))

    ## testing data
    ##encoding data
    #Object that 77 of 6744 sum up higher than 99.9
    dt[preference_scores_of_participant] = np.divide(dt[preference_scores_of_participant],100)
    dt[preference_scores_of_partner]=np.divide(dt[preference_scores_of_partner],100)
    '''

    x1_sum = np.sum(dt[preference_scores_of_participant],axis=1)
    print('\nFFFor particiant:',len([x for x in x1_sum if x <99.9]),' of ',len(dt))
    x1_sum = np.sum(dt[preference_scores_of_partner],axis=1)
    print('For partner:',len([x for x in x1_sum if x <99.9]),' of ',len(dt))
    x1_sum = np.sum(dt[rating_of_partner_from_participant],axis=1)
    print('For rating:',len([x for x in x1_sum if x <99.9]),' of ',len(dt),'\n')

    out:
    FFFor particiant: 77  of  6744
    For partner: 72  of  6744
    For rating: 6744  of  6744
    '''
    #lencode(dt)
    print('\n')
    #lencode(dt)
    #mean for dataset
    fmean(dt,preference_scores_of_participant)
    fmean(dt,preference_scores_of_partner)
    print('\n')

    #writing to a csv_file
    if len(sys.argv) < 3:
        print('Output file not found!!\nSystem creating outfile named %s and storing output data for you...\n'%'dating.csv')
        dt.to_csv('dating.csv',index=False)
    else:
        dt.to_csv(sys.argv[2],index=False)

    #### end MAIN


if __name__== "__main__":
    main()
