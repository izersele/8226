'''
author: izere gisele
Description:
 Split data set into training and testing set
Requirements:
 Python3, pandas, numpy, nbc.py
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
import sys

preference_scores_of_participant = ['attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important']
preference_scores_of_partner=['pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests']
continuous_valued_columns =['age','age_o','importance_same_race','importance_same_religion', 'pref_o_attractive', 'pref_o_sincere','pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious','pref_o_shared_interests','attractive_important', 'sincere_important', 'intelligence_important','funny_important', 'ambition_important', 'shared_interests_important','attractive' ,'sincere', 'intelligence','funny' ,'ambition','attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner','sports' ,'tvsports' ,'exercise' ,'dining','museums' ,'art' ,'hiking', 'gaming' ,'clubbing', 'reading', 'tv' ,'theater','movies', 'concerts', 'music' ,'shopping', 'yoga', 'interests_correlate','expected_happy_with_sd_people', 'like']
rating_of_partner_from_participant =['attractive_partner', 'sincere_partner','intelligence_parter', 'funny_partner', 'ambition_partner','shared_interests_partner']

#reading data from file
def load_csv(file):
    return pd.read_csv(file)

#partion data set into training and testing set
#rv: random states, frac: fraction in training set
def partitionning(dt,frac, rv):
    trainset= dt.sample(frac=frac, random_state=rv)
    testset= dt.loc[~dt.index.isin(trainset.index)]
    return trainset,testset

def main():
    if len(sys.argv) <2:
        print('File not found!!\n System exiting ...')
        sys.exit()

    dt = load_csv(sys.argv[1])
    #testing dataframe is not empty
    #print(dt.columns.values)
    if dt.empty:
        print('Empty file!!\n System exiting ...')
        sys.exit()

    xtrain, xtest = partitionning(dt,.2,47)
    #print(xtest)

    if len(sys.argv) < 4:
        f1 ='trainingSet.csv'
        f2 = 'testSet.csv'
        if len(sys.argv) < 3:
            print('Missing 2 arguments: File names not found!!')
            xtrain.to_csv(f1,index=False)
        else:
            print('Missing 1 arguments: File name not found!!')
        print('System created and stored output data for you in outfiles...\n')
        xtest.to_csv(f2,index=False)
    else:
        xtrain.to_csv(sys.argv[2],index=False)
        xtest.to_csv(sys.argv[3],index=False)
    ###end MAIN

if __name__== "__main__":
    main()
