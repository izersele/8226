#izere,gisele
#Decision tree implementation on wine quality data set
#Required Wine quality data set and python3 
import pandas as pd
import numpy as np
from math import log,exp
import operator
import copy
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split

#load data set
def load_csv():
	data = pd.read_csv('winequality-white.csv', sep=' ') #winequality-white.csv
	return data

#splits data set into 4 folds
def splitdataset(balance_data):
	X = []
	Y = []
	for i in range(len(balance_data)):
		X.append(balance_data[i][:-1])
		Y.append(balance_data[i][-1])

	folds_ = KFold(n_splits=4, shuffle=True, random_state=100)
	#print(folds_.get_n_splits(X,Y))

	return X, Y, folds_


def unique_vals(data,index):
	return set([x[index] for x in data])

##partition data on feature index of value val
def partition(data, label,index,val):
	_true, _false = [],[]
	_tl, _fl = [],[]
	for r,l in zip(data,label):
		f = r[:index]
		if index+1 < len(r):
			f.extend(r[index+1:])
		if r[index] >= val:
			_true.append(f)
			_tl.append(l)
		else:
			_false.append(f)
			_fl.append(l)
	return _true,_tl, _false,_fl
#counts occurance of each unique label
def _counts(label):
	c = {}
	for l in label:
		if l not in c:
			c[l] = 0
		c[l] +=1
	return c

#compute entropy
def entropy(data,label):
	c = _counts(label)
	n = len(data)
	entr = 0.0
	for  p in c:
		entr -= c[p]/n * log(c[p]/n, 2)
	return entr
#compute information gain
def infogain(xlf,ylf,xrt,yrt,entr):
	p = float(len(xlf)/(len(xlf)+len(xrt)))
	return entr - (p * entropy(xlf,ylf) + (1-p)* entropy(xrt,yrt))

#split data based on high information gain
def _split(data,label,N=0):
	curgain= entropy(data,label)
	bgain = 0
	nfeat = len(data[0]) if N == 0 else N
	feat, th =0,0
	for i in range(nfeat):
		vals = unique_vals(data,i)
		for val in vals:
			_trow,_tlab, _frow,_flab = partition(data,label,i,val)
			if len(_trow) == 0 or len(_frow) == 0: continue
			gain = infogain(_trow,_tlab, _frow,_flab,curgain)

			if gain > bgain: bgain,feat,th = gain,i,val
	return bgain,feat,th
#vote majority label
def Vote(labels):
	maj = 0
	ct = 0
	feat = 0
	for l in labels:
		if labels[l] > maj:
			feat = l
			maj = labels[l]
		ct += labels[l]
	return {feat:ct}

"""FUNCTION:BDT -- BUILD A DECISION TREE """
# @param : d: data samples, l: labels of y, f: features in data samples
def bdt(data,label,ftr =[]):
	N=len(ftr)
	bgain,feat,thresh = _split(data,label)
	if bgain <= 0.00:
		return _counts(label)
	attr = ftr[feat]
	x_lf,y_lf,x_rt,y_rt =partition(data,label,feat,thresh)

	if len(ftr)-1 == 0:
		return {'f':attr,'v':thresh,'tb':_counts(x_lf),'fb':_counts(x_rt)}
	_tbranch =bdt(x_lf,y_lf,ftr)
	_fbranch = bdt(x_rt,y_rt,ftr)
	tree = {'f':attr,'v':thresh,'tb':_tbranch,'fb':_fbranch}
	return tree

#return the maximum depth of the tree
def hparam(tree):
	if 'f' not in tree:
		return 0
	else:
		return 1+max(hparam(tree['tb']),hparam(tree['fb']))

#predict the label of x given the tree
def predicting(tree,x):
	#getting the leaves
	if 'f' not in tree:
		x = Vote(tree)
		for i in x : return i
	elif tree['v'] <= x[tree['f']]:
		return predicting(tree['tb'],x)
	else :
		return predicting(tree['fb'],x)
#prune the tree to reduce depth
def prune (tree,d =-1,f = -1):
	if 'f' not in tree :
		return tree

	elif (tree['f'] == f) or d == hparam(tree) :
		p = travel(tree['tb'])
		c = travel(tree['fb'])

		del tree['f']
		del tree['v']
		del tree['fb']
		del tree['tb']

		if len(c) >=1:
			for i in c:
				if i in p: p[i] += c[i]
				else:p[i] = c[i]

		v = Vote(p)
		#print("p: ",v)
		for i in p:
			tree[i] = p[i]

	else:
		prune(tree['tb'],d,f)
		prune(tree['fb'],d,f)

#traverse the tree to get labels in the leave nodes
def travel(tree):
	s = {}
	if 'f' not in tree:
		for i in tree:
			if i not in s: s[i] = tree[i]
	else:
		c = travel(tree['tb'])
		for i in c:
			if i not in s: s[i] = c[i]
			else: s[i] +=c[i]
		d = travel(tree['fb'])
		for i in d:
			if i not in s: s[i] = d[i]
			else: s[i] +=d[i]
	return s
#compute the bdt accuracy
def Accuracy(testSet,predictions):
	correct = 0
	for x in range(len(testSet)):
		if testSet[x] == predictions[x]:
			correct += 1
	return (correct/float(len(testSet))) * 100.0
#
def f1score(test,pred):
	#set of labels
	slabel = set(test) if len(set(test) ) >= len(set(pred)) else set(pred)
	fp = [pred.count(i) for i in slabel] # false positives
	fn = [test.count(i) for i in slabel] # false negatives

	tp = [sum([1 for i,j in zip(test,pred) if i==j and i == k]) for k in slabel] #true positives
	#print(fp,fn,tp)
	rec = Accuracy(test,pred)/100
	prec = sum([x/y for x,y in zip(tp,fp) if y > 0])/len(slabel)

	return 2*prec*rec/(prec+rec) *100.0 if prec > 0 or rec > 0 else 0

if __name__ == "__main__":
	s= load_csv()
	d = []
	header = s.columns.values[0].split(';')
	#print(header)
	for i in s.values:
		d.append([float(x) for x in i[0].split(';') ])

	X,Y,_fold = splitdataset(d)
	xn,xt,yn,yt = train_test_split(X, Y, test_size = 0.25, random_state = 100)
	feat = [z for z in range(len(xn[0]))]
	tree = bdt(xn,yn,feat)
	#print("Hyper parameter %d" %hparam(tree))
	"""" 4-fold cross validations """
	xtrain , ytrain= [],[]
	xtest, ytest = [],[]
	j = 1
	for tr,te in _fold.split(X,Y):
		X_train = []
		y_train = []
		X_test = []
		y_test = []

		# get the set of training and test sets
		for i in tr:
			X_train.append(X[i])
			y_train.append(Y[i])
		xtrain.append(X_train)
		ytrain.append(y_train)
		for i in te:
			X_test.append(X[i])
			y_test.append(Y[i])
		xtest.append(X_test)
		ytest.append(y_test)

	#scan the tree to find the best feature to prune on
	ctree =copy.deepcopy(tree)
	pred = [predicting(tree,x) for x in xtest[0]]

	dim = 0
	for i in range(hparam(tree)-1) :
		ac = Accuracy(ytest[0],pred)

		prune(ctree,i+1,-1)
		pred = [predicting(ctree,x) for x in xtest[0]]
		pracc = Accuracy(ytest[0],pred)
		if ac < pracc:
			dim= i+1
			ac = pracc
		ctree =copy.deepcopy(tree)
	## pruning at depth node which will increase accuracy than the other
	prune(ctree,dim,-1)
	tree =copy.deepcopy(ctree)

	print("Hyper parameter %d" %hparam(tree))
	f1,tf1,ac,tac = 0,0,0,0
	j=1
	for x,y,tx,ty in zip(xtrain ,ytrain, xtest, ytest):


		print("\nFold-%d"%j)
		j +=1
		pred = [predicting(tree,i) for i in x]
		f = f1score(y,pred)
		accur = Accuracy(y,pred)
		print("Training: F1 Score %.4f, Accuracy %.4f" % (f,accur))
		f1 +=f
		ac += accur
		pred = [predicting(tree,i) for i in tx]
		f = f1score(ty,pred)
		accur = Accuracy(ty,pred)
		tf1 +=f
		tac += accur
		print("Testing : F1 Score %.4f, Accuracy %.4f" % (f,accur))
		print("Validating : F1 Score %.4f, Accuracy %.4f" % (f,accur))

	print("\nAverage:")
	print("Training: F1 Score %.4f, Accuracy %.4f" % (f1/4,ac/4))
	print("Testing : F1 Score %.4f, Accuracy %.4f" % (tf1/4,tac/4))
	print("Validating : F1 Score %.4f, Accuracy %.4f" % (tf1/4,tac/4))
