#izera,gisele
#Implement Knn model on wine-quality data set
# required: python 3, pandas, sklearn

import pandas as pd
import numpy as np

import math
import operator
import random
from sklearn.model_selection import KFold
from numpy import linalg as la
from sklearn.model_selection import train_test_split

""" FUNCTION: LOAD_CSV """
def load_csv():
	data = pd.read_csv('winequality-white.csv', sep=' ') #winequality-white.csv
	return data

""" FUNCTION: SPLITDATASET """
def splitdataset(balance_data):
	X = []
	for i in range(len(balance_data)):
		X.append(balance_data[i])
	folds_ = KFold(n_splits=4, shuffle=True, random_state=100)
	return X, folds_

#random train test data split function definition
def euclideanDist(X, X_train):
    pro = np.dot(X,np.matrix(X_train).T)
    sum_square_test = np.square(X).sum(axis = 1)
    sum_square_train = np.square(X_train).sum(axis = 1)
    dists = np.sqrt(np.abs(-2 * pro + sum_square_train + np.matrix(sum_square_test).T))

    return(dists)
#cosine simularities
def cosSim(X, X_train):
    pro = np.dot(X,np.matrix(X_train).T)
    dists = 1-pro/(la.norm(X) * la.norm(X_train))

    return(dists)

def manH(X, X_train):
	dists = []
	for x in X:
		dist = np.sum(abs(np.subtract(x,X_train)),axis=1)
		dists.append(dist)

	return(np.matrix(dists))

#compute the accuracy and f1-score
def Accuracy(testSet, predictions):
	correct = 0
	for x in range(len(testSet)):
		if testSet[x][-1] == predictions[x]:
			correct += 1
	return (correct/float(len(testSet))) * 100.0

def f1_score(test,pred):
	#get labels
	ytest = [x[-1] for x in test]
	#set of labels
	slabel = set(ytest) if len(set(ytest) ) >= len(set(pred)) else set(pred)
	fp = [pred.count(i) for i in slabel]
	fn = [test.count(i) for i in slabel]

	tp = [sum([1 for i,j in zip(ytest,pred) if i==j and i == k]) for k in slabel]
	rec = Accuracy(test,pred)/100.0
	prec = sum([x/y for x,y in zip(tp,fp) if y > 0])/len(slabel)

	return 2*prec*rec/(prec+rec) *100 if prec > 0 or rec > 0 else 0

#KNN prediction and model training
def Voting(neigh):
	Votes = {}
	feat = 0
	for response in neigh:
		if response in Votes:
			Votes[response] += 1
		else:
			Votes[response] = 1

		if feat == 0:
			feat =response
		else:
			feat = response if Votes[response] > Votes[feat] else feat
	#sortedVotes = sorted(Votes, key=operator.itemgetter(0), reverse=True)
	return feat

#predict class of test points
def knn_predict(test, train,dist, k):
	ntest = len(test)
	y_train = []
	pred = [0]*ntest
	#get labels
	y_train = [x[-1] for x in train]

	for i in range(ntest):
		k_closest = []
		labels = []
		k_dist = np.asarray(np.argsort(dist[i,:])).reshape(-1) 	#print("k_dist:", k_dist.T[:k])
		labels = [y_train[k_dist[l]] for l in range(k)] #y_train[k_dist.T[l]]
		pred[i] = Voting(labels) ## to implement

	return pred

def bKnn(train,test,algorithm):
	distances = []
	for i in range(4):
		distances.append(algorithm(test[i],train[i]))
	r = 100
	vald = 100
	K = 2
	while r >=0.09:
		aerr= 0
		ferr = 0
		#print("\nKNN %d"%k)
		for i in range(4):
			pred =knn_predict(test[i], train[i],distances[i], K)
			t = Accuracy(test[i],pred)
			aerr +=(t)
			ferr += f1_score(test[i],pred)

		aerr /=4
		ferr /=4
		r = vald - aerr
		vald =aerr
		K +=1
		#print("Average performance : ",K-1, "Validation %.4f,%.4f"%(aerr,ferr))

	return K-1,vald,ferr

# computing result using the distance measure with best results
def Report(train,test,algorithm,k):
	aerr= 0
	ferr = 0
	for i in range(4):
		dist = algorithm(test[i],train[i])
		pred =knn_predict(test[i], train[i],dist,k)
		t = Accuracy(test[i],pred)
		ft = f1_score(test[i],pred)
		aerr +=(t)
		ferr += ft
		print("Fold - ", i+1)
		print ("Training: F1 Score: %.4f, Accuracy: %.4f "%(t,ft))
		print ("Validation: F1-score :%.4f, Accuracy: %.4f \n"%(t,ft))

	aerr /= 4
	ferr /= 4
	print("Average: ")
	print ("Training: F1 Score: %.4f, Accuracy: %.4f "%(aerr,ferr))
	print ("Validation: F1-score :%.4f, Accuracy: %.4f\n"%(aerr,ferr))

##Main function
if __name__ == "__main__":

	dt= load_csv() #getdata function call with csv file as parameter
	dataset = []
	for i in dt.values:
		dataset.append([float(x) for x in i[0].split(';') ])
	X,fold = splitdataset(dataset)
	#print(len(X))
	xtrain = []
	xtest = []
	Dist = []

	k = 1
	Accur = []
	for tr,te in fold.split(X):
		x_train = []
		x_test = []

		for i in tr:
			x_train.append(X[i])
		xtrain.append(x_train)

		for i in te:
			x_test.append(X[i])
		xtest.append(x_test)
	K, accur,f1 = bKnn(xtrain,xtest,euclideanDist)

	"""print("Measure:: Euclidean distance")
	print("K for KNN :: ", K)
	print ("Validation Accuracy :%.4f "%accur)
	print ("Validation f1-score :%.4f "%f1)"""
	# find best measure
	measure = [euclideanDist,K,accur,'Euclidean Distance']
	K, accur,f1 = bKnn(xtrain,xtest,manH)
	"""print("\nMeasure:: Manhattan distance")
	print("K for KNN :: ", K)
	print ("Validation Accuracy :%.4f "%accur)
	print ("Validation f1-score :%.4f "%f1)"""
	if measure[1] < K or measure[2] < accur:
		measure[0] =manH
		measure[1] = K
		measure[2] = accur
		measure[3] = 'Manhattan Distance'
	K, accur,f1 = bKnn(xtrain,xtest,cosSim)
	"""print("\nMeasure:: Cosine similarity distance")
	print("K for KNN :: ", K)
	print ("Validation Accuracy :%.4f "%accur)
	print ("Validation f1-score :%.4f "%f1)"""
	if measure[1] < K or measure[2] <= accur:
		measure[0] =cosSim
		measure[1] = K
		measure[2] = accur
		measure[3] = 'Cosine Similarity Distance'
	print('\n Hyper-parameters: \nK:',measure[1])
	print('Distance measure: ',measure[3])

	Report(xtrain,xtest,measure[0],measure[1])
